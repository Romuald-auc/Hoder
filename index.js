const Discord = require('discord.js');
const client = new Discord.Client({disableMentions: 'everyone'});
const eml = require('./src/cmds/eml');
const data = require('./src/data');
const bash = require('./src/bash');
const { Db, MongoClient } = require('mongodb');
const nameCollection = "Hoder";

// Dotnet env variable init
require('dotenv').config();

/** @type {Db} */
let mongoDB;

// start bot
client.login(process.env.Token);

// when error
client.on('error', err => {
    console.error(err);
});

/**
 * Callback quand le bot est fini de ce lancer
 */
client.on('ready', () => {
    console.log("Logged in as " + client.user.tag);

    const cl = new MongoClient(process.env.DbUrl, { useUnifiedTopology: true });
    cl.connect((error, result) => {
        if (error) console.log(error);

        mongoDB = result.db(process.env.DbName);
        if (mongoDB) InitSettings();
    });
});

/**
 * Permet de recevoir les informations quand
 * un message est envoyé dans un channel
 */
client.on('message', message => {
    if (message.content.startsWith('$')) {
        message.guild.members.cache.get(message.author.id)
        .fetch().then(member => {
            let a = data.get_data('sts', message.guild.id)["role-for-user"].split(',').find(l => member.roles.cache.find(rc => rc.id === l || rc.name === l) !== undefined);

            if (message.author.id === message.guild.ownerID || a !== undefined) {
                run_command(message);
            } else {
                message.channel.send({embed: { color: 0xee0000, title: "You not the owner" }});
            }
        });
    }
});

/**
 * Permet de supprimer un role quand une personne rajoute une réaction à
 * un message et qu'il contient un listener
 */
client.on('messageReactionRemove', (reaction, user) => {
    if (user.id === client.user.id) return;

    let role = eml.find_role(reaction.message.guild.id, reaction.message.id, reaction.emoji.identifier, reaction);
    if (role === undefined) return;

    reaction.message.guild.members.cache.find(gm => gm.user.id === user.id).roles.remove(role);
});

/**
 * Callback quand un message est supprimée
 */
client.on('messageDelete', message => {
    remove_listener(message.guild.id, message.id, message.channel.id);
});

/**
 * Callback quand plusieurs message sont supprimé
 */
client.on('messageDeleteBulk', messages => {
    let g = messages.first().guild.id, c = messages.first().channel.id;
    messages.forEach(m => remove_listener(g, m.id, c));
});

/**
 * Permet d'ajouter un role quand une personne rajoute une réaction à
 * un message et qu'il contient un listener
 */
client.on('messageReactionAdd', (reaction, user) => { 
    if (user.id === client.user.id) return;

    let role = eml.find_role(reaction.message.guild.id, reaction.message.id, reaction.emoji.identifier, reaction);
    if (role === undefined) return;

    reaction.message.guild.members.cache.find(gm => gm.user.id === user.id).roles.add(role);
});

/**
 * Appelle ce callback quand le bot rejoint un serveur
 */
client.on('guildCreate', guild => {
    mongoDB.collection(nameCollection).insertOne({
        serverID: guild.id,
        settings: {
            "used-by-all": true,
            "role-for-user": ""
        },
        listener: []
    }).then(r => console.log(guild.name + " is create"));

    data.set_data('sts', guild.id, {"role-for-user": ""});
    data.set_data('eml', guild.id, []);
});

/**
 * Appelle ce callback quand le bot quitte un serveur
 */
client.on('guildDelete', guild => {
    mongoDB.collection(nameCollection).deleteOne({serverID: guild.id});
});

/**
 * Permet de lancer une commande avec ces paramétres
 * @param {Discord.Message} message 
 */
function run_command(message) {
    let splites = message.content.split(' ');
    let command = splites[0];
    command = command.replace('$', '');
    splites.splice(0, 1);

    let id;
    do id = uuid();
    while (!data.add(id, {channel: message.channel, author: message.author}));

    bash.start_command(command, splites, id);
}

/**
 * Initialisation des paramètres des serveurs
 */
function InitSettings() {
    // Parcourt tout les serveurs et récupére les données des paramètres
    mongoDB.collection(nameCollection).find({}).toArray().then(arr => {
        client.guilds.cache.array().forEach(guild => {
            if (arr.find(el => el.serverID === guild.id) === undefined) {
                mongoDB.collection(nameCollection).insertOne({
                    serverID: guild.id,
                    settings: {
                        "role-for-user": ""
                    },
                    listeners: []
                }).then(() => {     
                    data.add('eml');               
                    data.set_data('eml', guild.id, []);
                    
                    data.add('sts');
                    data.set_data('sts', guild.id, {"role-for-user": ""});
                });
            }
        });

        arr.forEach(el => {
            let l = el.listeners;
            if (l !== null) {
                l.forEach(l => {
                    client.guilds.cache.get(el.serverID).channels.cache.filter(c => c.id === l.channel).forEach(c => {
                        c.messages.fetch(l.message);
                    });
                });
            }
            else
                el.listeners = [];

            client.guilds.cache.get(el.serverID).roles.fetch();

            data.add('eml');               
            data.set_data('eml', el.serverID, el.listeners);
                    
            data.add('sts');
            data.set_data('sts', el.serverID, {"role-for-user": el.settings['role-for-user']});
        });
    });

    // donne la référence au bash pour faciliter le changement des paramétres dans la base
    // de donnée
    data.add('mongodb', {db: mongoDB, name: nameCollection});

    client.user.setActivity({type: "LISTENING", name: "$command | $help"});
}

/**
 * Function qui permet de nettoyer la base de donnée
 */
function DropDatabase() {
    mongoDB.collection(nameCollection).drop().then(console.log("Drop is Finish"));
}

/**
 * Create a uuid
 */
function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

/**
 * remove listener
 * @param {string} id 
 * @param {string} message 
 * @param {string} channel 
 */
function remove_listener(id, message, channel) {
    let l = eml.find_listener(id, message, channel);
    if (l === undefined || l === "") return;

    eml.remove_listener(id, l);
}
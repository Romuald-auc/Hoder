const Discord = require('discord.js');
const data = require('../data');

/**
 * Function for role gestion
 * @param {string} command 
 * @param {string[]} params 
 * @param {Discord.TextChannel} channel
 */
function role(command, params, channel) {
    if (command === "add") {
        let value = params[0].replace("<@&", "");
        value = value.replace(">", "");

        let r = channel.guild.roles.cache.find(rl => rl.id === value || rl.name === value);
        if (r !== undefined) {
            value = r.name;
            add_role(channel.guild.id, value);
            channel.send({embed: {color: 0x00ee00, title: value + " is add"}});

            update_db(channel.guild.id);
        } else {
            channel.send({embed: {color: 0xee0000, title: value + " is not a role"}});
        }
    } else if (command === "remove") {
        let value = params[0].replace("<@&", "");
        value = value.replace(">", "");

        let r = channel.guild.roles.cache.find(rl => rl.id === value);
        if (r !== undefined) value = r.name;

        remove_role(channel.guild.id, value);
        channel.send({embed: {color: 0x00ee00, title: value + " is remove"}});

        update_db(channel.guild.id);
    } else if (command === "list") {
        let lst = setting_of_server(channel.guild.id, "role-for-user").split(',');
        
        let mes = "```Owner```";
        if (lst.length >= 1 && lst[0] !== '') {
            lst.forEach(el => {
                mes += "```" + el + "```";
            });
        }

        channel.send({embed: {color: 0x00ee00, title: "All role can use Hoder", description: mes}});
    } else if (command === "help") {
        channel.send({embed: {color: 0x00ee00, description: "`$sts role add {roleName}` Add role for use Hoder\n`$sts role remove {roleName}` Remove role for use Hoder\n`$sts role list` Print all role to use Hoder"}});
    } else {
        channel.send({embed: {color: 0xee0000, title: command + " wrong parameter"}});
    }
}

/**
 * Print help on channel
 * @param {string} command 
 * @param {string[]} params 
 * @param {Discord.TextChannel} channel  
 */
function help(command, params, channel) {
    channel.send({embed: {color: 0x00ee00, description: "`$sts role` Role management"}});
}

/**
 * Save information on database
 */
function update_db(serverID) {
    let mango = data.get('mongodb');
    mango.db.collection(mango.name)
    .updateOne({serverID: serverID}, { $set: { settings: find_settings(serverID) }}, (error, data) => {
        if (error) console.error(error);
    });
}

/**
 * @type {{help: help, role: role}}
 */
let func = {
    role: role,
    help: help
}

exports.lists_text = "`$sts` Change settings",

/**
 * Permet de lancer la function run
 * @param {string[]} params 
 * @param {string} id
 */
exports.run = (params, id) => {
    /** @type {Discord.TextChannel} */
    let channel = data.get_data(id, 'channel');

    let fu = func[params[0]];
    if (fu === undefined) {
        channel.send({embed: {color: 0xee0000, title: "`" + params[0]  + "` is not a good parameter"}})
        return;
    }

    let command = params[1];
    params = params.splice(2, params.length - 2);
    fu(command, params, channel);

    data.delete(id);
}


//------------------------ Function for data -----------------------//

/**
 * Permet de renvoyer les paramètres du serveur en question
 * @param {string} idServer 
 */
function find_settings(idServer) {
    var server = data.get('sts', idServer);
    if (server === undefined) {
        data.set_data('sts', idServer, settings);
        server = data.get('sts', idServer);
    }
    return server;
};

/**
 * Permet de récupérer la valeur du paramétre en fonction du serveur 
 * @param {string} idServer 
 * @param {*} parameter
 * 
 * @returns {*}
 */
function setting_of_server(idServer, parameter) {
    return data.get_data('sts', idServer)[parameter];
};

/**
 * Permet de changer la valeur du paramètre
 * @param {string} idServer 
 * @param {*} parameter 
 * @param {*} value 
 */
function update_parameter(idServer, parameter, value) {
    data.get_data('sts', idServer)[parameter] = value;
};

/**
 * Add role can used for the server
 * @param {string} idServer 
 * @param {string} value 
 */
function add_role(idServer, value) {
    let rol;
    if (setting_of_server(idServer, "role-for-user").length === 0) {
        rol = value;
    } else {
        let val = setting_of_server(idServer, "role-for-user").split(',');
        let add = false;
        val.forEach(el => {if (el === value) add = true;});

        if (!add) {
            rol = setting_of_server(idServer, "role-for-user") + "," + value;
        }
    }

    update_parameter(idServer, "role-for-user", rol);
};

/**
 * 
 * @param {string} idServer 
 * @param {string} value 
 */
function remove_role(idServer, value) {
    let val = setting_of_server(idServer, "role-for-user").replace(value, "");
    update_parameter(idServer, "role-for-user", val);
}

/**
 * All settings can change
 * @type {{"role-for-user": string}}
 */
settings = {
    "role-for-user": ""
};
const Discord = require('discord.js');
const data = require('../data');

/**
 * @type {string}
 */
exports.lists_text = "`$clear` Clear message";

/**
 * Delete last message with a count
 * @param {string[]} params
 * @param {string} id
 */
exports.run = (params, id) => {
    /** @type {Discord.TextChannel} */
    let channel = data.get_data(id, 'channel');
    /** @type {Discord.User} */
    let author = data.get_data(id, 'author');

    if (params.length > 1) {
        channel.send({embed: {color: 0xee0000, title: "To many arguments (see `$clear help`)"}});
        return;
    }

    let count = params.length === 0 ? 1 : parseInt(params[0]);
    count = max(min(count, 100), 1);

    if (params[0] === "help") {
        let message = "`$clear [count: 1]` Remove lasts messages with the number give (no more than 100 and no less than 1)";
        channel.send({embed: {color: 0x00ee00, title: "Clear Help", description: message}});
    } else {
        channel.messages.fetch({limit: count}).then(messages => {
            channel.bulkDelete(count);

            channel.send({embed: {color: 0xee0000, title: author.username + " clear " + min(count, messages.array().length) + " messages."}}).then(message => {
                message.delete({timeout: 2000});
            });
        });
    }

    data.delete(id);
}

/**
 * return the less number of message
 * @param {number} a 
 * @param {number} b 
 */
function min(a, b) {
    return a < b ? a : b;
}

/**
 * return the greater number of message
 * @param {number} a 
 * @param {number} b 
 */
function max(a, b) {
    return a < b ? b : a;
}
const Discord = require('discord.js');
const data = require('../data');

/**
 * @type {string}
 */
exports.lists_text = "`$eml` Listener Emoji";

/**
 * Change les informations pour les streameurs
 * @param {string[]} params
 * @param {string} id
 */
exports.run = (params, id) => {
    /** @type {Discord.TextChannel} */
    let channel = data.get_data(id, 'channel');
    
    if (params.length === 0) {
        channel.send({ embed: {color: 0xee0000, title: "```$eml``` need parameter (seed ```$eml help```)"}});
        return;
    }

    let action = params[0];
    let ids = channel.guild.id, listenerName = params[1];
    params = params.splice(2, params.length - 2);

    switch (action) {
        case "init": init(ids, params, channel,listenerName); break;
        case "add": add(ids, params, channel, listenerName); break;
        case "list": list(ids, channel); break;
        case "help": help(channel); break;
        case "remove": remove(ids, listenerName, channel);
        default: break;
    }

    data.delete(id);
}

/**
 * List all 
 * @param {string} id 
 * @param {Discord.TextChannel} channel 
 */
function list(id, channel) {
    let ls = get_all_listeners(id);
    if (ls.length === 0)
        channel.send({embed: { color: 0xee0000, title: "Empty" }});
    else {
        let message = "";
        ls.forEach(l => {
            message += "`" + l.listenerName + "` | "
        });

        message = message.slice(0, message.length - 2);
        channel.send({embed: {description: message, color: 0x00ee00, title: "List"}});
    }
}

/**
 * Print all help information
 * @param {*} channel 
 */
function help(channel) {
    let message = "`$eml init {listenerName} [url]` Create a listener with the message just above this command\n";
    message += "`$eml add {listenerName} {roleName}:{emojiName} ...` Add a emoji associate with a role to a listener\n";
    message += "`$eml remove {listenerName}` Remove listener and all reactions\n";
    message += "`$eml list` Print all listener of the current server/guild";
    channel.send({embed: {description: message, color: 0x00ee00, title: "All Commands"}});
}

/**
 * Remove listener and all reactions on discord channel
 * @param {string} id 
 * @param {string} listenerName 
 * @param {Discord.TextChannel} channel 
 */
function remove(id, listenerName, channel) {
    if (listenerName === "" || listenerName === undefined) {
        channel.send({ embed: {color: 0xee0000, title: "Give a listener name to remove"}});
        return;
    }

    let c = get_channel_id(id, listenerName);
    let m = get_message_id(id, listenerName);

    if (c === undefined || m === undefined) {
        channel.send({ embed: {color: 0xee0000, title: "Cannot delete a listener that does not exist"}});
        return;
    }

    if (remove_listener(id, listenerName)) {
        channel.guild.channels.cache.get(c).messages.cache.get(m).reactions.removeAll();
        channel.send({ embed: {color: 0x00ee00, title: "Deletion is a success"}});
    }
}

/**
 * Init
 * @param {string} id 
 * @param {string[]} params 
 * @param {Discord.TextChannel} channel 
 */
function init(id, params, channel, listenerName) {
    if (params.length > 1) {
        channel.send({embed: {color: 0xee0000, title: "Too many arguments"}});
        return;
    }

    if (params.length === 1 && is_url(params[0])) {
        let infos = params[0].split('/');

        if (infos.length !== 7) {
            channel.send({embed: {color: 0xee0000, description: "The url does not match"}});
            return;
        }

        let mes = infos[infos.length - 1], ch = infos[infos.length - 2];
        channel.guild.channels.cache.get(ch).messages.fetch(mes).then(message => {
            if (message.id === mes)
                add_listener(id, mes, ch, listenerName);
        });
    } else {
        channel.messages.fetch({limit: 4}).then(messages => {
            add_listener(id, messages.array()[1].id, channel.id, listenerName);
        });
    }

    channel.messages.cache.get(channel.lastMessageID).delete();
}

/**
 * Add role attribute
 * @param {string} id 
 * @param {string[]} params 
 * @param {Discord.TextChannel} channel 
 */
function add(id, params, channel, listenerName) {
    if (params.length < 1) return;

    let chan = get_channel_id(id, listenerName);
    let mes = get_message_id(id, listenerName);

    channel.guild.fetch().then(guild => {
        guild.channels.cache.find(ch => ch.id === chan).messages
        .fetch(mes)
        .then(message => {
            for (let i = 0; i < params.length; i++) {
                let p = params[i];
                p = p.replace("<@&", "");
                p = p.replace(">", "");
                p = p.replace("<:", "");

                let splice = p.split(':');
                let nameRole = splice[0], emojiIdentifier = splice[1];

                let emojis = channel.guild.emojis.cache.find(em => em.name === emojiIdentifier);
                if (emojis !== undefined) emojiIdentifier = emojis.identifier;

                nameRole = channel.guild.roles.cache.find(r => r.name === nameRole || r.id === nameRole).id;

                add_association(message.guild.id, listenerName, nameRole, emojiIdentifier);
                    
                message.react(emojiIdentifier);
            }
        })
    });

    channel.messages.cache.find(ms => ms.id === channel.lastMessageID).delete();
}

/**
 * Return true if string is a url
 * @param {string} url 
 */
function is_url(url) {
    return url.indexOf("https://discord.com/channels/") === 0;
}


//------------------------ Function for data -----------------------//

/**
 * Permet de savoir si on peut rajouter le role à la personne
 * en fonction de si on trouve un listener et avec l'emojie déjà ajouté
 * @param {*} id
 * @param {*} message
 * @param {string} emojiIdentifier
 * @param {Discord.MessageReaction} reaction
 */
exports.find_role = (id, message, emojiIdentifier, reaction) => {
    if (data.get_data('eml', id) === undefined) return undefined;

    let l = data.get_data('eml', id).find(l => l.message === message);
    if (l === undefined) return undefined;

    let as = l.associations.find(el => el.emojiIdentifier === emojiIdentifier || el.emojiIdentifier === reaction.emoji.name);
    if (as === undefined) return undefined;

    return as.nameRole;
}

/**
 * Return listener name if found
 * @param {string} id 
 * @param {string} message 
 * @param {string} channel 
 */
exports.find_listener = (id, message, channel) => {
    if (data.get_data('eml', id) === undefined) return undefined;

    let l = data.get_data('eml', id).find(m => m.message === message && m.channel === channel);
    if (l === undefined) return undefined;

    return l.listenerName;
}

/**
 * Recupére l'id du message
 * @param {string} id 
 * @param {string} listenerName 
 */
function get_message_id(id, listenerName) {
    if (data.get_data('eml', id) === undefined) return;

    let l = data.get_data('eml', id).find(l => l.listenerName === listenerName);
    if (l === undefined) return undefined;

    return l.message;
}

/**
 * Recupére l'id du message
 * @param {string} id 
 * @param {string} listenerName 
 */
function get_channel_id(id, listenerName) {
    if (data.get_data('eml', id) === undefined) return;

    let l = data.get_data('eml', id).find(l => l.listenerName === listenerName);
    if (l === undefined) return undefined;

    return l.channel;
}

/**
 * Ajoute un listener sur un message d'un serveur
 * @param {string} id 
 * @param {string} message 
 * @param {string} channel 
 * @param {string} listenerName
 */
function add_listener(id, message, channel, listenerName) {
    if (data.get_data('eml', id) === undefined) return;

    if (data.get_data('eml', id).find(l => l.message === message && l.channel === channel && l.listenerName === listenerName) !== undefined)
        return;

        data.get_data('eml', id).push({message: message, channel: channel, listenerName: listenerName, associations: []});

    let mango = data.get('mongodb');
    mango.db.collection(mango.name).updateOne({serverID: id}, {$set: {listeners: data.get_data('eml', id)}});
}

/**
 * Ajoute un listener
 * @param {listener} listener 
 */
function set_listener(listener) {
    data.get('eml').push(listener);
}

/**
 * Permet d'ajouter une association à un message déjà existant
 * @param {string} id 
 * @param {string} listenerName
 * @param {string} nameRole 
 * @param {string} emojiIdentifier 
 */
function add_association(id, listenerName, nameRole, emojiIdentifier) {
    if (data.get_data('eml', id) === undefined) return;

    let l = data.get_data('eml', id).find(l => l.listenerName === listenerName);
    if (l === undefined) return;

    l.associations.push({nameRole, emojiIdentifier});

    let mango = data.get('mongodb');
    mango.db.collection(mango.name).updateOne({serverID: id}, {$set: {listeners: data.get_data('eml', id)}});
}

/**
 * Add server
 * @param {string} serverID 
 * @param {listener[]} listeners 
 */
function add_server(serverID, listeners) {
    data.set_data('eml', serverID, listeners);
}

/**
 * return all listener of server
 * @param {*} serverID 
 */
function get_all_listeners(serverID) {
    if (data.get_data('eml', serverID) === undefined) return undefined;

    return data.get_data('eml', serverID);
}

/**
 * Remove listener
 * @param {string} id 
 * @param {string} name 
 */
function remove_listener(id, name) {
    if (data.get_data('eml', id) === undefined) return false;

    let pos = data.get_data('eml', id).findIndex(l => l.listenerName === name);
    if (pos === -1) return false;

    data.get_data('eml', id).splice(pos - 1, 1);

    let mango = data.get('mongodb');
    mango.db.collection(mango.name).updateOne({serverID: id}, {$set: {listeners: data.get_data('eml', id)}});

    return true;
};

/**
 * remove listener
 * @param {string} id 
 * @param {string} name 
 */
exports.remove_listener = (id, name) => {
    remove_listener(id, name);
}
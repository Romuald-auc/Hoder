const Discord = require('discord.js');
const command = require('../bash');
const data = require('../data');

/**
 * @type {string}
 */
exports.lists_text = "`$command` | `$help` Print All Command",

/**
 * Function for print all command
 * @param {string[]} params 
 * @param {string} id 
 */
exports.run = (params, id) => {
    /** @type {Discord.TextChannel} */
    let channel = data.get_data(id, 'channel');

    if (params.length > 0) {
        channel.send({
            embed: {
                color: 0xee0000,
                title: "To mush parameter"
            }
        });

        return;
    }
    
    // create description for message
    let message = "";
    Object.keys(command.get_commands()).forEach(key => {
        message += command.get_commands()[key].lists_text + '\n';
    });

    // send all command on channel
    channel.send({
        embed: {
            color: 0x00ee00,
            title: "All Commands",
            thumbnail: "images/discord.svg",
            description: message
        }
    });

    data.delete(id);
}
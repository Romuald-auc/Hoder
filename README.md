HoderBattle
======
![](https://img.shields.io/badge/Discordjs-12.5.1-purple)
![](https://img.shields.io/badge/MongoDB-3.6.3-blue)
![](https://img.shields.io/badge/Pipeline-Pass-green)

Création d'un bot à tout faire. Allant de la gestion des rôles grâce à l'utilisation des émojies à la suppression de message.

## Index
1. [Commandes](#commandes)
2. [Informations](#informations)
3. [Technologies utilisés](#technos)
4. [Hébergeur](#hébergeur)
4. [Ajouter sur Discord](#discord)

## Commandes
Chaque méthode hormis `$command` et `$help` possédent un paramètre `help` qui permet d'avoir une aide approfondie.

* `$command` ou `$help` va permettre d'afficher les commandes possibles et un cour résumé sur sont utilité.
* `$sts` sert à la gestion des paramètres de Hoder sur le serveur.
* `$eml` va permettre de créer des écoutes sur un message en associant un émojie à un rôle. Si l'émojie est enlever ou ajouter il supprime ou ajoute le role en question à la personne.
* `$clear` permet de supprimer N message dans le channel envoyé si aucun nombre n'est mit en supprime 5 par défaut.
* `$info` affiche des informations sur Hoder.

## Informations
Le bot devra avoir les droits administrateurs pour que toute les fonctionnalités fonctionnes.

## Technos
- NodeJS {15.8.0}
- MongoDB {3.6.3}
- DiscordJS {12.5.1}

## Hébergeur
Heroku est le site qui héberge le bot dans sa version gratuite

## Discord
[Ajouter Hoder a votre Server](https://discord.com/oauth2/authorize?&client_id=784064700681945158&scope=bot&permissions=8)
